FROM node:17-alpine3.12

EXPOSE 3000

RUN mkdir -p /usr/app

COPY app/ /usr/app/
WORKDIR /usr/app

RUN npm install
CMD ["node", "server.js"]