# Jenkins CI/CD pipeline for Node.js project

## Intro

Node.js based app with Jenkins CI/CD pipeline configuration files.

## Overview

*   [x] **Can be modified and extended**
*   [x] **Tested**

### Requirements

* Jenkins v2.303.3+
* Docker
* jenkins-shared-lib-node-project from https://gitlab.com/vladgrazhdan/jenkins.git

### Install

Use git to clone this repository locally.

### Usage

Jenkins pipeline.
