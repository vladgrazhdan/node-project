#!/usr/bin/env groovy

library identifier: 'jenkins-shared-lib-node-project@master', retriever: modernSCM(
    [$class: 'GitSCMSource',
     remote: 'https://gitlab.com/vladgrazhdan/jenkins-shared-lib-node-project.git',
     credentialsId: 'gitlab-creds'
    ]
)

pipeline {
    agent any

    tools {
        nodejs 'my-nodejs'
    }
    parameters {
        choice(name: 'VERSION', choices: ['patch', 'minor', 'major'], description: 'select version number to increment')
    }
    stages {
        stage ('increment version') {
            steps {
                dir('app') {
                    script {
                        incrementVer VERSION
                    }
                }
            }
        }
        stage ('app test') {
            steps {
                dir('app') {
                    script {
                        testApp()
                    }
                }
            }
            post {
                failure {
                    script {
                        error 'Test failed, exiting...'
                    }
                }
            }
        }
        stage ('build Docker image') {
            when {
                expression {
                    BRANCH_NAME == 'main'
                }
            }
            steps {
                script {
                    buildImage IMAGE_NAME
                }
            }
        }
        stage ('commit version update') {
            when {
                expression {
                    BRANCH_NAME == 'main'
                }
            }
            steps {
                script{
                    withCredentials([usernamePassword(credentialsId: 'gitlab-creds', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        sh 'git config --global user.email "jenkins@example.com"'
                        sh 'git config --global user.name "jenkins"'

                        sh 'git status'
                        sh 'git branch'
                        sh 'git config --list'

                        sh 'git remote set-url origin https://$USER:$PASS@gitlab.com/vladgrazhdan/node-project.git'
                        sh 'git add .'
                        sh 'git commit -m "ci: version bump"'
                        sh 'git push origin HEAD:master'
                    }
                }
            }
        }

        stage ('deploy Docker image to EC2') {
            when {
                expression {
                    BRANCH_NAME == 'main'
                }
            }
            steps {
                script {
                    sshagent(['ec2-server']) {
                    def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME}"
                    sh "scp -o StrictHostKeyChecking=no server-cmds.sh ec2-user@52.65.82.101:/home/ec2-user"
                    sh "scp docker-compose.yaml ec2-user@52.65.82.101:/home/ec2-user"
                    sh "ssh ec2-user@52.65.82.101 ${shellCmd}"

                    }
                }
            }
        }
    }
}